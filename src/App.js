import React, {useState, useEffect} from 'react';
import './App.css';
import {db, auth} from './firebase';

import Post from './Post';
import ImageUpload from './ImageUpload';

import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button, Input } from '@material-ui/core';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 250,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  }
  
}));


function App() {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);

  const [posts, setPosts] = useState([]);
  const [open, setOpen] = useState(false);
  const [openSignIn, setOpenSignIn] = useState(false)
  const [username, setusername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);

  useEffect(() => {
    
      // state no tiene memoria after refresh, esto de 
      // abajo si guarda los datos de auth
      const unsubscribe = auth.onAuthStateChanged((authUser) => {
        if (authUser) {
          // user has logged in...
          console.log(authUser);
          setUser(authUser);
        } else {
          //user has logged out..
          setUser(null);
        }
      });

      return () => {
        // perform cleanup de los datos
        unsubscribe();
      }
  }, [user, username]); 

  useEffect(() => {
    // aca es donde corre el codigo.
    db.collection('posts').onSnapshot(snapshot => {
      // cada vez que hay post nuevo este codigo corre
      setPosts(snapshot.docs.map(doc => ({
        id: doc.id,
        post: doc.data()
      })))
    })
  }, []); 

  const signUp = (event) => {
    event.preventDefault();

    auth
    .createUserWithEmailAndPassword(email, password)
    .then((authUser) => {
      return authUser.user.updateProfile({
        displayName: username
      })
    })
    .catch((error) => alert(error.message))

    setOpen(false);
  }

  const signIn = (event) => {
    event.preventDefault();

    auth
      .signInWithEmailAndPassword(email, password)
      .catch((error) => alert(error))

    setOpenSignIn(false);
  }

  return (
    <div className="App">

    
      <Modal
        open={open}
        onClose={() => setOpen(false)}
      >
        {
          <div style={modalStyle} className={classes.paper}>

            

            <form className="app__signup" >
              
              <img className="app__headerImage" 
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Instagram_logo.svg/200px-Instagram_logo.svg.png" 
              alt=""/>

              <Input 
                placeholder="username"
                type="text"
                value={username}
                onChange={(e) => setusername(e.target.value)}
              />
              <Input 
                placeholder="email"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Input 
                placeholder="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />

              <Button type="submit" onClick={signUp} >Sign Up</Button>
              
            </form>
            
          </div>
        }

      </Modal>

    {/* second modal */}
      <Modal

        open={openSignIn}
        onClose={() => setOpenSignIn(false)}
      >
        {
          <div style={modalStyle} className={classes.paper}>

            

            <form className="app__signup" >
              
              <img className="app__headerImage" 
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Instagram_logo.svg/200px-Instagram_logo.svg.png" 
              alt=""/>

              
              <Input 
                placeholder="email"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Input 
                placeholder="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />

              <Button type="submit" onClick={signIn} >Sign In</Button>
              
            </form>
            
          </div>
        }

      </Modal>

      <div className="app__header">
        <img className="app__headerImage" 
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Instagram_logo.svg/200px-Instagram_logo.svg.png" 
        alt=""/>

      {user ? (
        <Button onClick={() => auth.signOut()} >Logout</Button>
      ):
      (
      <div className="app__loginContainer">
        <Button onClick={() => setOpenSignIn(true)} >Sign In</Button>
        <Button onClick={() => setOpen(true)} >Sign Up</Button>
      </div>
      )
      }

      </div>

      
      
      
      <div className="app__posts">
        {
          posts.map(({id, post}) => (
            <Post key={id} 
                  postId={id} 
                  user={user}
                  username={post.username} 
                  caption={post.caption} 
                  imageUrl={post.imageUrl}/>
          ))
        }
      </div>
      
    

    {/* POSTS */}
    {/* POSTS */}


    {user?.displayName ? (
      <ImageUpload username={user.displayName}/>
    ): (
      <h3 className="app__imageUpload">You need to Login to upload</h3>
    )}
    </div>
  );
}

export default App;
