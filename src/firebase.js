import firebase from 'firebase';
 

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyApWGIF_NOmhGfTMeYbtopAARfZTxgCCyU",
    authDomain: "instagram-clone-c4e85.firebaseapp.com",
    databaseURL: "https://instagram-clone-c4e85.firebaseio.com",
    projectId: "instagram-clone-c4e85",
    storageBucket: "instagram-clone-c4e85.appspot.com",
    messagingSenderId: "216082479776",
    appId: "1:216082479776:web:022ab184455c17fe2827c5",
    measurementId: "G-Z931JSWYBZ"
})

 // Initialize Firebase

 const db = firebaseApp.firestore();
 const auth = firebase.auth();
 const storage = firebase.storage();

 export {db, auth, storage};