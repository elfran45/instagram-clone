# Instagram Clone  
  
An Instagram Clone using React and Firebase taught by the team at **Clever Programming**. It has a signUp-signIn, post image and comment functionality.  
![Screenshot_from_2020-08-05_12-57-22](/uploads/403b0473cdd9902e4b1484dd8286ba82/Screenshot_from_2020-08-05_12-57-22.png)
  

## Following This Course  
[Link To CrashCourse](https://www.youtube.com/watch?v=f7T48W0cwXM&feature=youtu.be&list=WL&t=11061)  
  
## Technologies  
  
- React  
- React Hooks  
- MaterialUI  
- Firebase
- Firebase Authentication
- Firebase Database
- Firebase Storage